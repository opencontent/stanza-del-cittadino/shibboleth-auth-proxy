<SPConfig xmlns="urn:mace:shibboleth:3.0:native:sp:config"
          xmlns:conf="urn:mace:shibboleth:3.0:native:sp:config"
          xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion"
          xmlns:samlp="urn:oasis:names:tc:SAML:2.0:protocol"    
          xmlns:md="urn:oasis:names:tc:SAML:2.0:metadata"
          clockSkew="180">

  <OutOfProcess tranLogFormat="%u|%s|%IDP|%i|%ac|%t|%attr|%n|%b|%E|%S|%SS|%L|%UA|%a" />
  
  <!--
  By default, in-memory StorageService, ReplayCache, ArtifactMap, and SessionCache
  are used. See example-shibboleth2.xml for samples of explicitly configuring them.
  -->

  <!--
  The ApplicationDefaults element is where most of Shibboleth's SAML bits are defined.
  Resource requests are mapped by the RequestMapper to an applicationId that
  points into to this section (or to the defaults here).
  -->
  <ApplicationDefaults entityID="https://sp.example.org/shibboleth"
                       REMOTE_USER="eppn persistent-id targeted-id">

    <!--
    Controls session lifetimes, address checks, cookie handling, and the protocol handlers.
    You MUST supply an effectively unique handlerURL value for each of your applications.
    The value defaults to /Shibboleth.sso, and should be a relative path, with the SP computing
    a relative value based on the virtual host. Using handlerSSL="true", the default, will force
    the protocol to be https. You should also set cookieProps to "https" for SSL-only sites.
    Note that while we default checkAddress to "false", this has a negative impact on the
    security of your site. Stealing sessions via cookie theft is much easier with this disabled.
    -->
    <Sessions lifetime="28800" timeout="3600" relayState="ss:mem"
              checkAddress="false" handlerSSL="true" cookieProps="https">

      <!--
      Configures SSO for a default IdP. To allow for >1 IdP, remove
      entityID property and adjust discoveryURL to point to discovery service.
      (Set discoveryProtocol to "WAYF" for legacy Shibboleth WAYF support.)
      You can also override entityID on /Login query string, or in RequestMap/htaccess.
      -->
      <SSO entityID="https://idp.example.org/idp/shibboleth"
           discoveryProtocol="SAMLDS" discoveryURL="https://ds.example.org/DS/WAYF">
        SAML2 SAML1
      </SSO>

      <!-- SAML and local-only logout. -->
      <Logout>SAML2 Local</Logout>

      <!-- Extension service that generates "approximate" metadata based on SP configuration. -->
      <Handler type="MetadataGenerator" Location="/Metadata" signing="false"/>

      <!-- Status reporting service. -->
            <Handler type="Status" Location="/Status" acl="127.0.0.1 ::1"/>

      <!-- Session diagnostic service. -->
      <Handler type="Session" Location="/Session" showAttributeValues="true"/>

      <!-- JSON feed of discovery information. -->
      <Handler type="DiscoveryFeed" Location="/DiscoFeed"/>
  
      <!--
      <LogoutInitiator type="Chaining" Location="/Logout" asynchronous="false"> 
        <LogoutInitiator type="SAML2" template="bindingTemplate.html"/> 
      </LogoutInitiator>

  
      <md:SingleLogoutService Location="/SLO/SOAP" Binding="urn:oasis:names:tc:SAML:2.0:bindings:SOAP"/> 
      <md:SingleLogoutService Location="/SLO/Redirect" conf:template="bindingTemplate.html" Binding="urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect"/> 
      <md:SingleLogoutService Location="/SLO/POST" conf:template="bindingTemplate.html" Binding="urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST"/> 
      <md:SingleLogoutService Location="/SLO/Artifact" conf:template="bindingTemplate.html" Binding="urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Artifact"/>
      -->
      
    </Sessions>

    <!--
    Allows overriding of error template information/filenames. You can
    also add attributes with values that can be plugged into the templates.
    -->
    <Errors supportContact="root@localhost"
            helpLocation="/about.html"
            styleSheet="/shibboleth-sp/main.css"/>
    
    <!-- Example of remotely supplied batch of signed metadata. -->
    <MetadataProvider type="XML" url="https://vm-siracsso.comune.genova.it/sirac-sso/metadata"
          backingFilePath="/var/cache/shibboleth/sirac-sso-metadata.xml" reloadInterval="7200">
    </MetadataProvider>
  
    <!--
    <MetadataProvider type="XML" uri="http://federation.org/federation-metadata.xml"
          backingFilePath="federation-metadata.xml" reloadInterval="7200">
        <MetadataFilter type="RequireValidUntil" maxValidityInterval="2419200"/>
        <MetadataFilter type="Signature" certificate="fedsigner.pem"/>
    </MetadataProvider>
    -->

    <!-- Example of locally maintained metadata. -->
    <!--
    <MetadataProvider type="XML" file="partner-metadata.xml"/>
    -->

    <!-- Map to extract attributes from SAML assertions. -->
    <!--<AttributeExtractor type="XML" validate="true" reloadChanges="false" path="attribute-map.xml"/>-->
	<AttributeExtractor type="XML" validate="true" reloadChanges="false" path="attribute-map-siracsso.xml"/>
    
    <!-- Use a SAML query if no attributes are supplied during SSO. -->
    <AttributeResolver type="Query" subjectMatch="true"/>

    <!-- Default filtering policy for recognized attributes, lets other data pass. -->
    <!--<AttributeFilter type="XML" validate="true" path="attribute-policy.xml"/>-->

    <!-- Simple file-based resolvers for separate signing/encryption keys. 
    <CredentialResolver type="File" use="signing"
        key="sp-signing-key.pem" certificate="sp-signing-cert.pem"/>
    <CredentialResolver type="File" use="encryption"
        key="sp-encrypt-key.pem" certificate="sp-encrypt-cert.pem"/>
    -->
    <CredentialResolver type="File" key="sp-key.pem" certificate="sp-cert.pem" keyInfoMask="4"/>

    <!--
    The default settings can be overridden by creating ApplicationOverride elements (see
    the https://wiki.shibboleth.net/confluence/display/SHIB2/NativeSPApplicationOverride topic).
    Resource requests are mapped by web server commands, or the RequestMapper, to an
    applicationId setting.
    
    Example of a second application (for a second vhost) that has a different entityID.
    Resources on the vhost would map to an applicationId of "admin":
    -->
    <ApplicationOverride id="shib-sp-test-java" 
                         entityID="https://vm-siracsso.comune.genova.it/shib-sp-test-java/shibboleth" 
                         REMOTE_USER="cf"
                         signing="true" encryption="false"
                         homeURL="https://vm-siracsso.comune.genova.it/shib-sp-test-java">

      <Sessions lifetime="28800" timeout="3600" checkAddress="false"
                handlerURL="/shib-sp-test-java/protected/Shibboleth.sso" handlerSSL="true"
                exportLocation="/GetAssertion" exportACL="127.0.0.1"
                idpHistory="false" idpHistoryDays="7" cookieProps="; path=/shib-sp-test-java; secure; HttpOnly">
      
        <SessionInitiator type="Chaining" Location="/Login" isDefault="true" id="Login"
                          entityID="https://vm-siracsso.comune.genova.it/sirac-sso/metadata">  
      
          <SessionInitiator type="SAML2" acsIndex="1" template="bindingTemplate.html"><!-- outgoingBindings="urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST">-->
            <samlp:AuthnRequest IssueInstant="2017-04-07T14:37:47.093Z" ID="s2a423837a35edf50a7ed265787a2485e474ef9c" AttributeConsumingServiceIndex="1" Version="2.0">
              <samlp:NameIDPolicy AllowCreate="true" Format="urn:oasis:names:tc:SAML:2.0:nameid-format:transient"/>
              <samlp:RequestedAuthnContext Comparison="minimum">
                <!--<saml:AuthnContextClassRef xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion">urn:oasis:names:tc:SAML:2.0:ac:classes:PasswordProtectedTransport</saml:AuthnContextClassRef>-->
                <!--<saml:AuthnContextClassRef xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion">urn:oasis:names:tc:SAML:2.0:ac:classes:SecureRemotePassword</saml:AuthnContextClassRef>-->
                <!--<saml:AuthnContextClassRef xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion">urn:oasis:names:tc:SAML:2.0:ac:classes:TimeSyncToken</saml:AuthnContextClassRef>-->
                <!--<saml:AuthnContextClassRef xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion">urn:oasis:names:tc:SAML:2.0:ac:classes:SmartCard</saml:AuthnContextClassRef>-->
                <saml:AuthnContextClassRef xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion">https://www.spid.gov.it/SpidL1</saml:AuthnContextClassRef>
                <!--<saml:AuthnContextClassRef xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion">https://www.spid.gov.it/SpidL2</saml:AuthnContextClassRef>-->
                <!--<saml:AuthnContextClassRef xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion">https://www.spid.gov.it/SpidL3</saml:AuthnContextClassRef>-->
              </samlp:RequestedAuthnContext>
            </samlp:AuthnRequest>
          </SessionInitiator>
          
        </SessionInitiator>

      </Sessions>
    
      <Errors supportContact="root@localhost"
              helpLocation="/about.html"
              styleSheet="/shibboleth-sp/main.css"
              requestURL="/shib-sp-test-java" />
      
      <RelyingParty Name="https://vm-siracsso.comune.genova.it/sirac-sso/metadata" requireSignedAssertions="true" signing="true" signingAlg="http://www.w3.org/2001/04/xmldsig-more#rsa-sha256" digestAlg="http://www.w3.org/2001/04/xmlenc#sha256"/>

      <Notify Channel="front" Location="https://vm-siracsso.comune.genova.it/shib-sp-test-java/logoutNotificationReceiver.jsp" />

      <MetadataProvider type="XML" url="https://vm-siracsso.comune.genova.it/sirac-sso/metadata"
                        backingFilePath="/var/cache/shibboleth/sirac-sso-metadata.xml" reloadInterval="7200">
      </MetadataProvider>
          
    </ApplicationOverride>

    <ApplicationOverride id="shib-sp-test-java-eidas" 
                         entityID="https://vm-siracsso.comune.genova.it/shib-sp-test-java-eidas/shibboleth" 
                         REMOTE_USER="cf"
                         signing="true" encryption="false"
                         homeURL="https://vm-siracsso.comune.genova.it/shib-sp-test-java-eidas">

      <Sessions lifetime="28800" timeout="3600" checkAddress="false"
                handlerURL="/shib-sp-test-java-eidas/protected/Shibboleth.sso" handlerSSL="true"
                exportLocation="/GetAssertion" exportACL="127.0.0.1"
                idpHistory="false" idpHistoryDays="7" cookieProps="; path=/shib-sp-test-java-eidas; secure; HttpOnly">
        
        <SessionInitiator type="Chaining" Location="/Login" isDefault="true" id="Login"
                          entityID="https://vm-siracsso.comune.genova.it/sirac-sso/metadata">  
        
          <SessionInitiator type="SAML2" acsIndex="1" template="bindingTemplate.html"><!-- outgoingBindings="urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST">-->
            <samlp:AuthnRequest IssueInstant="2017-04-07T14:37:47.093Z" ID="s2a321b84f44edf50a7ed265787a2485e474ef9c" AttributeConsumingServiceIndex="1" Version="2.0">
              <samlp:NameIDPolicy AllowCreate="true" Format="urn:oasis:names:tc:SAML:2.0:nameid-format:transient"/>
              <samlp:RequestedAuthnContext Comparison="minimum">
                <!--<saml:AuthnContextClassRef xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion">urn:oasis:names:tc:SAML:2.0:ac:classes:PasswordProtectedTransport</saml:AuthnContextClassRef>-->
                <!--<saml:AuthnContextClassRef xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion">urn:oasis:names:tc:SAML:2.0:ac:classes:SecureRemotePassword</saml:AuthnContextClassRef>-->
                <!--<saml:AuthnContextClassRef xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion">urn:oasis:names:tc:SAML:2.0:ac:classes:TimeSyncToken</saml:AuthnContextClassRef>-->
                <!--<saml:AuthnContextClassRef xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion">urn:oasis:names:tc:SAML:2.0:ac:classes:SmartCard</saml:AuthnContextClassRef>-->
                <saml:AuthnContextClassRef xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion">https://www.spid.gov.it/SpidL1</saml:AuthnContextClassRef>
                <!--<saml:AuthnContextClassRef xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion">https://www.spid.gov.it/SpidL2</saml:AuthnContextClassRef>-->
                <!--<saml:AuthnContextClassRef xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion">https://www.spid.gov.it/SpidL3</saml:AuthnContextClassRef>-->
              </samlp:RequestedAuthnContext>
            </samlp:AuthnRequest>
          </SessionInitiator>
          
        </SessionInitiator>

      </Sessions>
      
      <Errors supportContact="root@localhost"
              helpLocation="/about.html"
              styleSheet="/shibboleth-sp/main.css"
              requestURL="/shib-sp-test-java-eidas" />
      
      <RelyingParty Name="https://vm-siracsso.comune.genova.it/sirac-sso/metadata" requireSignedAssertions="true" signing="true" signingAlg="http://www.w3.org/2001/04/xmldsig-more#rsa-sha256" digestAlg="http://www.w3.org/2001/04/xmlenc#sha256"/>

      <Notify Channel="front" Location="https://vm-siracsso.comune.genova.it/shib-sp-test-java-eidas/logoutNotificationReceiver.jsp" />

      <MetadataProvider type="XML" url="https://vm-siracsso.comune.genova.it/sirac-sso/metadata"
                        backingFilePath="/var/cache/shibboleth/sirac-sso-metadata.xml" reloadInterval="7200"/>
			
			<AttributeExtractor type="XML" validate="true" reloadChanges="false" path="attribute-map-eidas.xml"/>

    </ApplicationOverride>


    <ApplicationOverride id="segnalazioni" 
                         entityID="https://segnalazioni.comune.genova.it/sirac/auth/shibboleth" 
                         REMOTE_USER="emailAddress"
                         signing="true" encryption="false"
                         homeURL="https://segnalazioni.comune.genova.it/">

      <Sessions lifetime="28800" timeout="3600" checkAddress="false"
                handlerURL="/sirac/auth/Shibboleth.sso" handlerSSL="true" relayState="ss:mem"
                exportLocation="/GetAssertion" exportACL="127.0.0.1" 
                idpHistory="false" idpHistoryDays="7" cookieProps="; path=/; secure; HttpOnly">

        <!-- Extension service that generates "approximate" metadata based on SP configuration. -->
        <Handler type="MetadataGenerator" Location="/Metadata" signing="false"/>

        <SessionInitiator type="Chaining" Location="/Login" isDefault="true" id="Login"
                          entityID="https://vm-siracsso.comune.genova.it/sirac-sso/metadata">  
                                  
          <SessionInitiator type="SAML2" acsIndex="1" template="bindingTemplate.html"><!-- outgoingBindings="urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST">-->
            <samlp:AuthnRequest IssueInstant="2013-04-17T15:37:47.093Z" ID="s2a549b39b35edf50a7ed265787a2485e454ef9c" Version="2.0">
              <samlp:NameIDPolicy AllowCreate="false" Format="urn:oasis:names:tc:SAML:2.0:nameid-format:transient"/>
              <samlp:RequestedAuthnContext Comparison="minimum">
                <saml:AuthnContextClassRef xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion">https://www.spid.gov.it/SpidL1</saml:AuthnContextClassRef>
              </samlp:RequestedAuthnContext>
            </samlp:AuthnRequest>
          </SessionInitiator>          
          
        </SessionInitiator>
        
      </Sessions>            
      
      <Notify Channel="front" Location="https://segnalazioni.comune.genova.it/sirac/slo" />  

    </ApplicationOverride>
	
    <ApplicationOverride id="proponiti" 
                         entityID="https://proponiti.comune.genova.it/sirac/auth/shibboleth" 
                         REMOTE_USER="emailAddress"
                         signing="true" encryption="false"
                         homeURL="https://proponiti.comune.genova.it/">

      <Sessions lifetime="28800" timeout="3600" checkAddress="false"
                handlerURL="/sirac/auth/Shibboleth.sso" handlerSSL="true" relayState="ss:mem"
                exportLocation="/GetAssertion" exportACL="127.0.0.1" 
                idpHistory="false" idpHistoryDays="7" cookieProps="; path=/; secure; HttpOnly">

        <!-- Extension service that generates "approximate" metadata based on SP configuration. -->
        <Handler type="MetadataGenerator" Location="/Metadata" signing="false"/>

        <SessionInitiator type="Chaining" Location="/Login" isDefault="true" id="Login"
                          entityID="https://vm-siracsso.comune.genova.it/sirac-sso/metadata">  
                                  
          <SessionInitiator type="SAML2" acsIndex="1" template="bindingTemplate.html"><!-- outgoingBindings="urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST">-->
            <samlp:AuthnRequest IssueInstant="2013-04-17T15:37:47.093Z" ID="s2a549b39b35edf50a7ed265787a2485e454efb3" Version="2.0">
              <samlp:NameIDPolicy AllowCreate="false" Format="urn:oasis:names:tc:SAML:2.0:nameid-format:transient"/>
              <samlp:RequestedAuthnContext Comparison="minimum">
                <saml:AuthnContextClassRef xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion">https://www.spid.gov.it/SpidL1</saml:AuthnContextClassRef>
              </samlp:RequestedAuthnContext>
            </samlp:AuthnRequest>
          </SessionInitiator>          
          
        </SessionInitiator>
        
      </Sessions>            
      
      <Notify Channel="front" Location="https://proponiti.comune.genova.it/sirac/slo" />  

    </ApplicationOverride>
    
   </ApplicationDefaults>
  
  <!-- Policies that determine how to process and authenticate runtime messages. -->
  <SecurityPolicyProvider type="XML" validate="true" path="security-policy.xml"/>

  <!-- Low-level configuration about protocols and bindings available for use. -->
  <ProtocolProvider type="XML" validate="true" reloadChanges="false" path="protocols.xml"/>

</SPConfig>
