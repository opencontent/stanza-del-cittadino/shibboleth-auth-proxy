FROM centos:7

# add Shibboleth repo
COPY ./compose_conf/shibboleth/etc/yum.repos.d/shibboleth.repo /etc/yum.repos.d/

# Modify YUM repository configuration to use CentOS Vault
RUN sed -i 's/mirrorlist/#mirrorlist/g' /etc/yum.repos.d/CentOS-* \
    && sed -i 's|#baseurl=http://mirror.centos.org|baseurl=https://vault.centos.org|g' /etc/yum.repos.d/CentOS-*

# install dependencies
RUN yum install -y \
        httpd \
        java-1.8.0-openjdk-headless \
        mod_ssl \
        shibboleth \
        unzip \
    && yum -y clean all


# add application paths
COPY ./compose_conf/shibboleth/opt/shibboleth-sp /opt/shibboleth-sp

# add configurations
COPY ./compose_conf/shibboleth/etc/shibboleth/ /etc/shibboleth/
COPY ./compose_conf/apache/conf.d/ /etc/httpd/conf.d/


# copy bootstrap script
COPY ./compose_conf/shibboleth/usr/local/bin/ /usr/local/bin/
RUN chmod +x \
    /usr/local/bin/docker-bootstrap.sh

COPY ./public /var/www/html/public

# run it
EXPOSE 80 443
CMD ["docker-bootstrap.sh"]

